const nodemailer = require('nodemailer');

let smtpConfig = {
    host: 'localhost',
    port: 587,
    tls: {rejectUnauthorized: false},
    secure: false, // upgrade later with STARTTLS
    auth: {
        user: 'user1',
        pass: 'password1'
    }
  };
  
var mailOptions = {
    from: 'Maulana Shalihin ✔ <maulana@cektransfer.com>', // sender address
    to: 'maulana@1pesan.com', // list of receivers
    subject: 'halo guys', // Subject line
    html:  '<p>dengan saya maulana shalihin</p>', // html body
    headers: {
        'channel': 'webmail'
    },
};
var transporter =  nodemailer.createTransport(smtpConfig);
transporter.sendMail(mailOptions,  function(error, info){
    if (error) {
        console.log('--------------- email from  --------------')
        console.log(mailOptions.from)
        console.log('--------------- send to --------------')
        console.log(mailOptions.to)
        console.error(error);
        // put the failed message item back to queue
        return;
    }

    console.log('--------------- send to --------------')
    console.log(mailOptions.to)
    console.log('--------------- subject --------------')
    console.log(mailOptions.subject)

    console.log('Delivered message %s', info.messageId);
    // remove message item from the queue
});