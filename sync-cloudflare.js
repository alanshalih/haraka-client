const fs = require('fs');
var axios = require('axios');
const publicIp = require('public-ip');
var ProgressBar = require('progress');
var linode_auth = "Bearer d7083be49e46b11b0d4a6a117bb207a57306542c28e0af4ce3a9a3f52b00e9b7"

fs.readFile("/root/haraka-email/config/me", "utf8", async function (err, data) {

    var ip = (await publicIp.v4());
    console.log('sync with cloudflare DNS Manager ...')
    if (data) {
        axios.post('https://api.cloudflare.com/client/v4/zones/0fad9df962f8a95875e953f00f8e0a68/dns_records', {
            type: 'A',
            name: data,
            content: ip
        }, {
            headers: {
                'X-Auth-Key': '7015d3a1e89cc8e0b3ff67a2b82c83ce5728b',
                'X-Auth-Email': 'jualansandi@gmail.com'
            }
        }).then(response => {
            // res.send(response.data)
            axios.get('https://api.linode.com/v4/linode/instances', {
                headers: {
                    'Authorization': linode_auth
                }
            }).then(response => {
                var Linode = response.data.data.find(item => {
                    return item.ipv4.includes(ip);
                })
                if (Linode) {


                    var bar = new ProgressBar(':bar', {
                        total: 100
                    });
                    var Interval = setInterval(function () {
                        bar.tick();
                    }, 100);

                    setTimeout(() => {
                        axios.put('https://api.linode.com/v4/linode/instances/' + Linode.id + '/ips/' + ip, {
                            "rdns": data
                        }, {
                            headers: {
                                'Authorization': linode_auth
                            },
                        }).then(response => {
                            console.log('\n\n');
                            console.log(response.data)
                          
                        }, error => {
                            console.log(error)
                        })
                        clearInterval(Interval)
                    }, 10 * 1000)
                }



            })
        }, error => {
            console.log('data is exist')
        })
    }
});