var ProgressBar = require('progress');
var bar = new ProgressBar(':bar', { total: 100 });
var timer = setInterval(function () {
  bar.tick();
}, 100);