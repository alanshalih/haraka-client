const fs = require('fs');
const axios = require('axios')
var os = require("os");

axios.get('https://app.1pesan.com/api/sending-domain').then(response => {

    response.data.forEach(item => {
        fs.mkdir('/root/haraka-email/config/dkim/' + item.name, {
            recursive: true
        }, (err) => {

            if (item.dkim_public && item.dkim_private) {
                fs.writeFile("/root/haraka-email/config/dkim/" + item.name + '/public', item.dkim_public, function (err) {
                    if (err) {
                        return console.log(err);
                    }
                });

                fs.writeFile("/root/haraka-email/config/dkim/" + item.name + '/private', item.dkim_private, function (err) {
                    if (err) {
                        return console.log(err);
                    }
                });

                fs.writeFile("/root/haraka-email/config/dkim/" + item.name + '/selector', '1pesan' + os.EOL, function (err) {
                    if (err) {
                        return console.log(err);
                    }
                });

                console.log('dkim sert for '+item.name+' successfully written')
            }

        });
    })

})