const express = require('express')
const app = express()
const port = 1453
var bodyParser = require('body-parser')
const nodemailer = require('nodemailer');
const fs = require('fs');
var os = require("os");
var cors = require('cors');
const { exec } = require('child_process');
let messages = [];

fs.readFile('./queue', function read(err, data) {
  if (err) {
      throw err;
  }
  console.log(data)
  //messages = JSON.parse(data);
        // Or put the next step in a function and invoke it
});


let smtpConfig = {
  host: 'localhost',
  port: 587,
  pool: true,
  maxConnections: 100,
  tls: {
    rejectUnauthorized: false
  },
  secure: false, // upgrade later with STARTTLS
  auth: {
    user: 'user1',
    pass: 'password1'
  }
};




var transporter = nodemailer.createTransport(smtpConfig);



// parse application/x-www-form-urlencoded
// in latest body-parser use like below.
app.use(bodyParser.urlencoded({
  parameterLimit: 100000,
  extended: true,
  limit: '50mb',
}));

app.use(bodyParser.json({limit: '50mb', extended: true}))


// parse application/json
app.use(bodyParser.json())


app.use(cors());







app.post('/process-emails', function (req, res) {

  res.send('OK')

  var mailOptions = {
    to: req.body.to,
    from: req.body.from,
    subject: req.body.subject,
    text: req.body.text,
    html: req.body.html,
    headers: {
      'key-id': req.body.messageId,
    }
  };

  if (req.body.replyTo) {
    mailOptions.replyTo = req.body.replyTo;
  }

  if (req.body.sender) {
    mailOptions.sender = req.body.sender;
  }

  messages.push(mailOptions)
  

})

app.post('/send-bulk', function (req, res) {

  res.send('OK')

  var data = req.body.data;
  
  
  data.forEach(item=>{
    var mailOptions = {
      to: item.to,
      from: item.from,
      subject: item.subject,
      text: item.text,
      html: item.html,
      headers: {
        'key-id': item.messageId,
        'send_from' : req.body.send_from ? req.body.send_from : "app.1pesan.com",
      }
    };
  
    if (item.replyTo) {
      mailOptions.replyTo = item.replyTo;
    }
  
    if (item.sender) {
      mailOptions.sender = item.sender;
    }
  
    messages.push(mailOptions)
  })


 
  

})

var counter = {};

setInterval(()=>{
    if(transporter.isIdle() && messages.length) {
      SendEmail();
    }
},100);



function SendEmail()
{
  
  var data = messages.shift();
      
  console.log('--------------- send to --------------')
  console.log(data.to)
  console.log('--------------- subject --------------')
  console.log(data.subject)
  
  transporter.sendMail(data, function (error, info) {
    if(error)
    {
      //  data.counter = data.counter ? data.counter++ : 1;
      //  if(data.counter < 3)
      //  {
      //    messages.push(data)
      //  }
      console.log(error)
    }else{
      console.log('Delivered message %s', info.messageId);
    }  
     
  });
}




app.post('/check-server', function (req, res) {
  res.send('ok');
});


app.get('/', (req, res) => res.send('Hello World!'))


app.get('/is-online', (req, res) => {
  exec('node /root/haraka-client/port-opened.js', (err, stdout, stderr) => {
    if (err) {
      res.send('error')
      return;
    }
    res.send(stdout)
  });
});

setInterval(()=>{
  exec('node ./port-opened.js', (err, stdout, stderr) => {
    
    if(stdout.includes('inactive'))
    {

      exec('node /root/haraka-email/killport.js && haraka -c /root/haraka-email', (err, stdout, stderr) => {
        console.log('restarting haraka')
      });
    }
  });

// echo "0 0 * * * root rm /etc/log/haraka.log" >> /etc/crontab
// echo "0 0 * * * root rm /var/log/haraka.log" >> /etc/crontab
// echo "0 0 * * 0 root pm2 flush" >> /etc/crontab

// clear all log
  exec('cd /root && du -s', (err, stdout, stderr) => {
    
      var disk_usage = parseInt(stdout.split(' ')[0])
      if(disk_usage > 1000000)
      {

        exec('pm2 flush', (err, stdout, stderr) => {
          console.log('flush pm2')
        });
      
      }
  });

  exec('cd /var/log && du -s', (err, stdout, stderr) => {
    
    var disk_usage = parseInt(stdout.split(' ')[0])
    if(disk_usage > 1000000)
    {

      exec('echo "" > /var/log/haraka.log', (err, stdout, stderr) => {
        console.log('flush pm2')
      });
    
    }
});


exec('cd /etc/log && du -s', (err, stdout, stderr) => {
    
  var disk_usage = parseInt(stdout.split(' ')[0])
  if(disk_usage > 1000000)
  {

    exec('echo "" > /etc/log/haraka.log', (err, stdout, stderr) => {
      console.log('flush pm2')
    });
  
  }
});

},5*60*1000)


app.get('/get-me', (req, res) => {

  fs.readFile("/root/haraka-email/config/me", "utf8", function (err, data) {
    if (err) throw err;

    res.send(data);
  });
})




app.post('/register-dkim', function (req, res) {


  if (req.body.domain) {

    fs.mkdir('/root/haraka-email/config/dkim/' + req.body.domain, {
      recursive: true
    }, (err) => {

      if (req.body.dkim_public && req.body.dkim_private && req.body.dkim_selector) {
        fs.writeFile("/root/haraka-email/config/dkim/" + req.body.domain + '/public', req.body.dkim_public, function (err) {
          if (err) {
            return console.log(err);
          }
        });

        fs.writeFile("/root/haraka-email/config/dkim/" + req.body.domain + '/private', req.body.dkim_private, function (err) {
          if (err) {
            return console.log(err);
          }
        });

        fs.writeFile("/root/haraka-email/config/dkim/" + req.body.domain + '/selector', req.body.dkim_selector + os.EOL, function (err) {
          if (err) {
            return console.log(err);
          }
        });

        res.send('dkim sert successfully written')
      } else {
        res.status(500).send('DKIM Sign Needed!')

      }



    });

  } else {
    res.status(500).send('Domain Needed!')
  }

})
process.on('SIGINT',doSomething); 

process.on ('SIGTERM', doSomething);


function doSomething(){

  fs.writeFile("./queue", JSON.stringify(messages), function(err) {
      if(err) {
          return console.log(err);
      }
      console.log('close event')
      process.exit();
  }); 
 
}


app.listen(2019, () => console.log(`Example app listening on port 2019!`))