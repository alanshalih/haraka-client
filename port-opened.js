var net = require('net');
var server = net.createServer();

var port = 587;

server.once('error', function(err) {
  if (err.code === 'EADDRINUSE') {
    // port is currently in use
    console.log(port+' is active')
  }
});

server.once('listening', function() {
  // close the server if listening doesn't fail
  console.log(port+' is inactive')
  server.close();
});

server.listen(port);